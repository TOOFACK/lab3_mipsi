import sklearn 
from sklearn.linear_model import LogisticRegression
import numpy as np
import joblib

if __name__ ==  "__main__":
    
    logreg=LogisticRegression(C=0.8)
    X_train = np.load('X_train.npy')
    y_train = np.load('y_train.npy')
    logreg.fit(X_train,y_train)
    y_train_pred=logreg.predict(X_train)
    np.save('predict.txt', y_train_pred)
    
    # save model
    joblib.dump(logreg, 'logreg.bin')
    
    
    

