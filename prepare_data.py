from sklearn import datasets
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split


if __name__ == "__main__":
    iris = datasets.load_iris()


    X=iris.data
    y=iris.target
    
    
    X_train,X_test,y_train,y_test=train_test_split(X,y,test_size=0.3, random_state=4)
    
    np.save("X_train", X_train)
    np.save("y_train", y_train)
    
    np.save("X_test", X_test)
    np.save("y_test", y_test)

