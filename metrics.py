import joblib
import numpy as np
from sklearn import metrics
from sklearn.metrics import ConfusionMatrixDisplay
import matplotlib.pyplot as plt




if __name__ ==  "__main__":
    logreg_model = joblib.load("logreg.bin")
    X_test = np.load('X_test.npy')
    y_test = np.load('y_test.npy')
    y_test_pred=logreg_model.predict(X_test)
    acc = metrics.accuracy_score(y_test,y_test_pred)

    f1_mera = metrics.f1_score(y_test, y_test_pred, average='macro')
    print(acc)
    print(f1_mera)

    with open("acc.txt", "w") as outfile:
        outfile.write("Accuracy: " + str(acc) + "\n")

    
    with open("f1_mera.txt", "w") as outfile:
        outfile.write("F1: " + str(f1_mera) + "\n")
    # Plot it
    disp = ConfusionMatrixDisplay.from_estimator(
        logreg_model, X_test, y_test, normalize="true", cmap=plt.cm.Blues
    )
    plt.savefig("plot.png")
