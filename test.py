import joblib
import numpy as np
from sklearn import metrics

if __name__ ==  "__main__":
    logreg_model = joblib.load("logreg.bin")
    X_test = np.load('X_test.npy')
    y_test = np.load('y_test.npy')
    y_test_pred=logreg_model.predict(X_test)
    print(metrics.accuracy_score(y_test,y_test_pred))
    
    assert np.allclose(metrics.accuracy_score(y_test,y_test_pred), 0.97, rtol=0.01)

    